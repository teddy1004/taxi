//go:generate mockgen -destination billingmock/biller.go -package billingmock github.com/teddy1004/taxi/pkg/billing Biller
package billing

import "github.com/teddy1004/taxi/model"

// Biller provides func for calculate order fee
type Biller interface {
	// CalculateFeeCents calculate fee based on order / taxi info
	CalculateFeeCents(order *model.Order, taxi *model.Taxi) (int, error)
}

// priceOpt is for identifying time range (left-close right-open) and its starting price
type priceOpt struct {
	start                     int
	end                       int
	startingPriceCents        int
	normalWaterMarkPriceCents int
	highWaterMarkPriceCents   int
}

type defaultBiller struct {
	DayTimePrice   *priceOpt
	NightTimePrice *priceOpt
	// HighWaterMark is distance threshold for distance above this value would charge extra money for inner-ring taxi,
	// default is 10KM
	HighWaterMark int
	// HighWaterMarkTaxiRingTypes is taxi ring types need extra money for distance over HighWaterMark
	HighWaterMarkTaxiRingTypes []model.RingType
	// NormalWaterMark is distance threshold for distance above this value whould charge extra money, default is 3KM
	NormalWaterMark int
}

// NewBiller initialize a default implementation of Biller
func NewDefaultBiller() Biller {
	return &defaultBiller{
		DayTimePrice: &priceOpt{
			start:                     6,
			end:                       23,
			startingPriceCents:        14 * 1000,
			normalWaterMarkPriceCents: 2500,
			highWaterMarkPriceCents:   3500,
		},
		NightTimePrice: &priceOpt{
			start:                     23,
			end:                       6,
			startingPriceCents:        18 * 1000,
			normalWaterMarkPriceCents: 3000,
			highWaterMarkPriceCents:   4700,
		},
		NormalWaterMark:            3,
		HighWaterMark:              10,
		HighWaterMarkTaxiRingTypes: []model.RingType{model.RingTypeInner},
	}
}

// CalculateFeeCents implements Biller.CalculateFeeCents on defaultBiller
func (bl *defaultBiller) CalculateFeeCents(order *model.Order, taxi *model.Taxi) (int, error) {
	err := order.Validate()
	if err != nil {
		return 0, err
	}

	sum := 0
	startingPrice, isDayTime := bl.calculateStartingPrice(order)
	sum += startingPrice

	var (
		normalWaterMarkPriceCents int
		highWaterMarkPriceCents   int
	)

	if isDayTime {
		normalWaterMarkPriceCents = bl.DayTimePrice.normalWaterMarkPriceCents
		highWaterMarkPriceCents = bl.DayTimePrice.highWaterMarkPriceCents
	} else {
		normalWaterMarkPriceCents = bl.NightTimePrice.normalWaterMarkPriceCents
		highWaterMarkPriceCents = bl.NightTimePrice.highWaterMarkPriceCents
	}

	// distance over 3KM, calculate extra price
	if order.Distance > bl.NormalWaterMark {
		sum += bl.calculateNormalWaterMarkPrice(order, normalWaterMarkPriceCents)
	}

	// distance over 10KM, calculate extra price
	if order.Distance > bl.HighWaterMark {
		sum += bl.calculateHighWaterMarkPrice(order, taxi, highWaterMarkPriceCents, normalWaterMarkPriceCents)
	}

	return sum, nil
}

// calculateStartingPrice based on rule
// 	- 6m - 23:59:59pm, 14rmb for distance within 3km
// 	- 23am - 05:59:59am, 18rmb for distance within 3km
// returns priceCents and isDayTime
func (bl *defaultBiller) calculateStartingPrice(order *model.Order) (int, bool) {
	// starting at day time
	if order.StartedAt.Hour() >= bl.DayTimePrice.start && order.StartedAt.Hour() < bl.DayTimePrice.end {
		return bl.DayTimePrice.startingPriceCents, true
	}

	// starting at night time
	return bl.NightTimePrice.startingPriceCents, false
}

// calculateNormalWaterMarkPrice calculate price for distance within normalWaterMark and highWaterMark
func (bl *defaultBiller) calculateNormalWaterMarkPrice(order *model.Order, pricePerKM int) int {
	totalKM := order.Distance / 1000
	if order.Distance%1000 != 0 {
		totalKM += 1
	}

	var normalDistanceKM int
	if totalKM > bl.HighWaterMark {
		normalDistanceKM = bl.HighWaterMark - bl.NormalWaterMark
	} else {
		normalDistanceKM = totalKM - bl.NormalWaterMark
	}
	if normalDistanceKM < 0 {
		return 0
	}

	return normalDistanceKM * pricePerKM
}

// calculateHighWaterMarkPrice calculate price for distance above highWaterMark
func (bl *defaultBiller) calculateHighWaterMarkPrice(order *model.Order, taxi *model.Taxi, pricePerKM int, normalPricePerKM int) int {
	distanceKM := order.Distance/1000 - bl.HighWaterMark
	if order.Distance%1000 != 0 {
		distanceKM += 1
	}

	if distanceKM < 0 {
		return 0
	}

	if bl.isChargeExtraForHighWaterMark(taxi) {
		return distanceKM * pricePerKM
	}

	return distanceKM * normalPricePerKM
}

// isChargeExtraForHighWaterMark returns bool value for if charge extra money for distance over HighWaterMark
func (bl *defaultBiller) isChargeExtraForHighWaterMark(taxi *model.Taxi) bool {
	for _, t := range bl.HighWaterMarkTaxiRingTypes {
		if t == taxi.RingType {
			return true
		}
	}

	return false
}
