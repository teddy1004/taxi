// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/teddy1004/taxi/pkg/billing (interfaces: Biller)

// Package billingmock is a generated GoMock package.
package billingmock

import (
	gomock "github.com/golang/mock/gomock"
	model "github.com/teddy1004/taxi/model"
	reflect "reflect"
)

// MockBiller is a mock of Biller interface
type MockBiller struct {
	ctrl     *gomock.Controller
	recorder *MockBillerMockRecorder
}

// MockBillerMockRecorder is the mock recorder for MockBiller
type MockBillerMockRecorder struct {
	mock *MockBiller
}

// NewMockBiller creates a new mock instance
func NewMockBiller(ctrl *gomock.Controller) *MockBiller {
	mock := &MockBiller{ctrl: ctrl}
	mock.recorder = &MockBillerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockBiller) EXPECT() *MockBillerMockRecorder {
	return m.recorder
}

// CalculateFeeCents mocks base method
func (m *MockBiller) CalculateFeeCents(arg0 *model.Order, arg1 *model.Taxi) (int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CalculateFeeCents", arg0, arg1)
	ret0, _ := ret[0].(int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CalculateFeeCents indicates an expected call of CalculateFeeCents
func (mr *MockBillerMockRecorder) CalculateFeeCents(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CalculateFeeCents", reflect.TypeOf((*MockBiller)(nil).CalculateFeeCents), arg0, arg1)
}
