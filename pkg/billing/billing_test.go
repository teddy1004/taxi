package billing

import (
	"os"
	"testing"
	"time"

	"github.com/teddy1004/taxi/model"
)

var biller Biller

func TestMain(m *testing.M) {
	biller = NewDefaultBiller()
	os.Exit(m.Run())
}

func Test_defaultBiller_CalculateFeeCents(t *testing.T) {
	type args struct {
		order *model.Order
		taxi  *model.Taxi
	}

	sixOClock, err := time.Parse(time.RFC3339, "2019-07-31T06:00:00.371Z")
	if err != nil {
		panic(err)
	}

	tests := []struct {
		name    string
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "invalid order #invalid distance",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   0,
					StartedAt:  sixOClock,
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "invalid order #invalid finished_at",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   0,
					StartedAt:  sixOClock,
					FinishedAt: sixOClock.Add(-1 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    0,
			wantErr: true,
		},
		{
			name: "Starting price 14, within 3 km (6am - 23pm)",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   5,
					StartedAt:  sixOClock,
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    14 * 1000,
			wantErr: false,
		},
		{
			name: "Starting price 18, within 3 km (23pm - 6am)",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   5,
					StartedAt:  sixOClock.Add(-1 * time.Hour),
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    18 * 1000,
			wantErr: false,
		},
		{
			name: "Starting at 7am, 4km, should charge 14 + 2.5 = 16.5rmb",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   4000,
					StartedAt:  sixOClock.Add(1 * time.Hour),
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    16500,
			wantErr: false,
		},
		{
			name: "Starting at 4am, 4km, should charge 18 + 3 = 21rmb",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   4000,
					StartedAt:  sixOClock.Add(-2 * time.Hour),
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    21000,
			wantErr: false,
		},
		{
			name: "Starting at 6am, 11km, should charge 14 + 2.5*7 + 3.5 = 35rmb for inner ring type taxi",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   11000,
					StartedAt:  sixOClock,
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    35000,
			wantErr: false,
		},
		{
			name: "Starting at 6am, 11km, should charge 14 + 2.5*7 + 2.5 = 34rmb for outer ring type taxi",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   11000,
					StartedAt:  sixOClock,
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: model.RingTypeOuter,
				},
			},
			want:    34000,
			wantErr: false,
		},
		{
			name: "Starting at 4am, 11km, should charge 18 + 3*7 + 4.7 = 43.7rmb for inner ring type taxi",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   11000,
					StartedAt:  sixOClock.Add(-2 * time.Hour),
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: 0,
				},
			},
			want:    43700,
			wantErr: false,
		},
		{
			name: "Starting at 4am, 11km, should charge 18 + 3*7 + 3 = 42rmb for outer ring type taxi",
			args: args{
				order: &model.Order{
					ID:         1,
					TaxiID:     1,
					Distance:   11000,
					StartedAt:  sixOClock.Add(-2 * time.Hour),
					FinishedAt: sixOClock.Add(2 * time.Hour),
					FeeCents:   0,
				},
				taxi: &model.Taxi{
					ID:       1,
					RingType: model.RingTypeOuter,
				},
			},
			want:    42000,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := biller.CalculateFeeCents(tt.args.order, tt.args.taxi)
			if (err != nil) != tt.wantErr {
				t.Errorf("CalculateFeeCents() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CalculateFeeCents() got = %v, want %v", got, tt.want)
			}
		})
	}
}
