//go:generate mockgen -destination storemock/storer.go -package storemock github.com/teddy1004/taxi/store Storer
package store

import (
	"sync"
	"sync/atomic"
	"time"

	"github.com/teddy1004/taxi/model"
)

// Storer is interface for data store
type Storer interface {
	CreateOrder(ringType model.RingType) *model.Order
	FinishOrder(o *model.Order)
	GetTaxi(id int32) *model.Taxi
	GetOrder(id int32) *model.Order
}

// MemoryStore is a very simple implementation of Storer, stores data in memory for simplicity
type MemoryStore struct {
	OrderStore map[int32]*model.Order
	TaxiStore  map[int32]*model.Taxi
	OrderIndex *int32
	TaxiIndex  *int32
	sync.Mutex
}

func NewDefaultStore() Storer {
	orderIdx := int32(0)
	taxiIdx := int32(0)

	return &MemoryStore{
		OrderStore: make(map[int32]*model.Order),
		TaxiStore:  make(map[int32]*model.Taxi),
		OrderIndex: &orderIdx,
		TaxiIndex:  &taxiIdx,
	}
}

// CreateOrder implements Storer.CreateOrder on MemoryStore
func (ms *MemoryStore) CreateOrder(ringType model.RingType) *model.Order {
	taxi := &model.Taxi{
		ID:       atomic.AddInt32(ms.TaxiIndex, 1),
		RingType: ringType,
	}
	order := &model.Order{
		ID:         atomic.AddInt32(ms.OrderIndex, 1),
		TaxiID:     taxi.ID,
		Distance:   0,
		StartedAt:  time.Now(),
		FinishedAt: time.Time{},
		FeeCents:   0,
	}

	ms.Lock()
	defer ms.Unlock()
	ms.OrderStore[order.ID] = order
	ms.TaxiStore[taxi.ID] = taxi

	return order
}

// FinishOrder implements Storer.FinishOrder on MemoryStore
func (ms *MemoryStore) FinishOrder(o *model.Order) {
	ms.Lock()
	defer ms.Unlock()

	ms.OrderStore[o.ID] = o
}

// GetTaxi implements Storer.GetTaxi on MemoryStore
func (ms *MemoryStore) GetTaxi(id int32) *model.Taxi {
	return ms.TaxiStore[id]
}

// GetOrder implements Storer.GetOrder on MemoryStore
func (ms *MemoryStore) GetOrder(id int32) *model.Order {
	return ms.OrderStore[id]
}
