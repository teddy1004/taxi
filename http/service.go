package http

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// Service wraps http.Server
type Service struct {
	*http.Server
	g    *gin.Engine
	addr string
}

// NewService creates http Service
func NewService(addr string, opts ...ServiceOption) *Service {
	s := &Service{
		g:    gin.Default(),
		addr: addr,
	}
	s.Server = &http.Server{
		Addr:              s.addr,
		Handler:           s.g,
		ReadHeaderTimeout: time.Second * 10,
		WriteTimeout:      time.Second * 10,
		MaxHeaderBytes:    1 << 20, // 1M
	}
	for _, opt := range opts {
		opt(s)
	}

	return s
}

// Run wraps http ListenAndServe
func (s *Service) Run() error {
	logrus.Infof("http server listen %s", s.addr)
	return s.ListenAndServe()
}

// Close may close or flush recourse
func (s *Service) Close() error {
	// flush...
	return s.Server.Close()
}
