package order

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/teddy1004/taxi/initializer"
	"github.com/teddy1004/taxi/model"
)

// startOrderParam is params for create order, currently we only need
// model.RingType
type startOrderParam struct {
	RingType model.RingType `json:"ringType"`
}

// finishOrderParam is params for finish order
type finishOrderParam struct {
	OrderID  int32 `json:"orderId"`
	Distance int   `json:"distance"`
}

// Start starts a new order and create a random taxi in memory
func Start(c *gin.Context) {
	var param startOrderParam
	if err := c.BindJSON(&param); err != nil {
		logrus.Error(err)
		return
	}

	o := initializer.Store.CreateOrder(param.RingType)
	c.JSON(http.StatusCreated, gin.H{
		"orderId":   o.ID,
		"startedAt": o.StartedAt.Unix(),
		"ringType":  param.RingType,
	})
}

// Finish finishes a order and calculate its total fee
func Finish(c *gin.Context) {
	var param finishOrderParam
	if err := c.BindJSON(&param); err != nil {
		logrus.Error(err)
		return
	}
	orderIDi64, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		logrus.Error(err)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	param.OrderID = int32(orderIDi64)

	order := initializer.Store.GetOrder(param.OrderID)
	if order == nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	taxi := initializer.Store.GetTaxi(order.TaxiID)
	if taxi == nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	order.Distance = param.Distance
	order.FinishedAt = time.Now()
	order.FeeCents, err = initializer.Biller.CalculateFeeCents(order, taxi)
	if err != nil {
		logrus.Error(err)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"orderId":    order.ID,
		"finishedAt": order.FinishedAt.Unix(),
		"feeCents":   order.FeeCents,
	})
}
