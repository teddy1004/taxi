package order

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/teddy1004/taxi/pkg/billing/billingmock"

	"github.com/teddy1004/taxi/model"

	"github.com/golang/mock/gomock"
	"github.com/teddy1004/taxi/initializer"
	"github.com/teddy1004/taxi/store/storemock"

	"github.com/stretchr/testify/assert"

	"github.com/gin-gonic/gin"
)

func performRequest(r http.Handler, method, path string, body io.Reader) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, body)
	req.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	return w
}

func TestStart(t *testing.T) {
	// Setup router for test
	r := gin.Default()
	r.POST("/api/orders", Start)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storer := storemock.NewMockStorer(ctrl)
	initializer.Store = storer
	o := &model.Order{
		ID:         1,
		TaxiID:     1,
		Distance:   0,
		StartedAt:  time.Now(),
		FinishedAt: time.Time{},
		FeeCents:   0,
	}
	storer.EXPECT().CreateOrder(gomock.Any()).Return(o)

	w := performRequest(r, "POST", "/api/orders", bytes.NewBuffer([]byte(`{"ringType": 2}`)))
	assert.Equal(t, http.StatusCreated, w.Code)

	var response map[string]interface{}
	err := json.Unmarshal([]byte(w.Body.String()), &response)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, float64(o.ID), response["orderId"].(float64))
	assert.Equal(t, float64(2), response["ringType"].(float64))
}

func TestFinish(t *testing.T) {
	// Setup router for test
	r := gin.Default()
	r.PUT("/api/orders/:id", Finish)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	storer := storemock.NewMockStorer(ctrl)
	initializer.Store = storer
	o := &model.Order{
		ID:         1,
		TaxiID:     1,
		Distance:   0,
		StartedAt:  time.Now().Add(-1 * time.Hour),
		FinishedAt: time.Time{},
		FeeCents:   0,
	}
	taxi := &model.Taxi{
		ID:       1,
		RingType: 0,
	}
	storer.EXPECT().GetOrder(o.ID).Return(o)
	storer.EXPECT().GetTaxi(taxi.ID).Return(taxi)

	biller := billingmock.NewMockBiller(ctrl)
	initializer.Biller = biller
	biller.EXPECT().CalculateFeeCents(o, taxi).Return(1000, nil)

	w := performRequest(r, "PUT", "/api/orders/1", bytes.NewBuffer([]byte(`{"distance": 4000}`)))
	assert.Equal(t, http.StatusOK, w.Code)

	var response map[string]interface{}
	err := json.Unmarshal([]byte(w.Body.String()), &response)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, float64(1000), response["feeCents"].(float64))
}
