package http

import (
	"github.com/teddy1004/taxi/http/handler/order"
)

// ServiceOption is used for plugable Service settings
type ServiceOption func(s *Service)

// WithGinRouteOption route plugin
func WithGinRouteOption() ServiceOption {
	return func(s *Service) {
		s.g.POST("/api/orders", order.Start)
		s.g.PUT("/api/orders/:id", order.Finish)
	}
}
