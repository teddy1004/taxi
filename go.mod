module github.com/teddy1004/taxi

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/golang/mock v1.3.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0
)
