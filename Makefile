.PHONY: test
test:
	CGO_ENABLED=0 go test -count=1 ./...

.PHONY: build
build:
	mkdir -p bin
	rm -fr bin/taxi
	CGO_ENABLED=0 go build -o bin/taxi cmd/httpserver/server.go

mock-gen:
	go generate ./...