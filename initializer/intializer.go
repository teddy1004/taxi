package initializer

import (
	"github.com/teddy1004/taxi/pkg/billing"
	"github.com/teddy1004/taxi/store"
)

var (
	Store  store.Storer
	Biller billing.Biller
)

func InitHTTPService() {
	initStorer()
	initBiller()
}

func initStorer() {
	Store = store.NewDefaultStore()
}

func initBiller() {
	Biller = billing.NewDefaultBiller()
}
