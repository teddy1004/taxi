package model

// RingType is ring type for taxi, include inner / middle / outer
type RingType int8

const (
	RingTypeInner RingType = iota
	RingTypeMiddle
	RingTypeOuter
)

// Taxi is model for taxi
type Taxi struct {
	ID       int32    `xorm:"'id' not null pk autoincr INT(11)"`
	RingType RingType `xorm:"ring_type"`
}
