package model

import (
	"testing"
	"time"
)

func TestOrder_Validate(t *testing.T) {
	type fields struct {
		ID         int32
		TaxiID     int32
		Distance   int
		StartedAt  time.Time
		FinishedAt time.Time
		FeeCents   int
	}
	now := time.Now()
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "valid",
			fields: fields{
				TaxiID:     1,
				Distance:   1,
				StartedAt:  now,
				FinishedAt: now.Add(2 * time.Hour),
				FeeCents:   0,
			},
			wantErr: false,
		},
		{
			name: "invalid finished_at",
			fields: fields{
				TaxiID:     1,
				Distance:   1,
				StartedAt:  now,
				FinishedAt: now.Add(-1 * time.Hour),
				FeeCents:   0,
			},
			wantErr: true,
		},
		{
			name: "invalid distance",
			fields: fields{
				TaxiID:     1,
				Distance:   0,
				StartedAt:  now,
				FinishedAt: now.Add(1 * time.Hour),
				FeeCents:   0,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &Order{
				ID:         tt.fields.ID,
				TaxiID:     tt.fields.TaxiID,
				Distance:   tt.fields.Distance,
				StartedAt:  tt.fields.StartedAt,
				FinishedAt: tt.fields.FinishedAt,
				FeeCents:   tt.fields.FeeCents,
			}
			if err := o.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
