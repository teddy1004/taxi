package model

import (
	"errors"
	"time"
)

var (
	OrderDistanceInvalidError   = errors.New("invalid distance for order")
	OrderFinishedAtInvalidError = errors.New("invalid finished_at for order")
)

// Order is model for order
type Order struct {
	ID     int32 `xorm:"'id' not null pk autoincr INT(11)"`
	TaxiID int32 `xorm:"taxi_id"`
	// Distance is driving distance, unit is meter
	Distance   int       `xorm:"distance"`
	StartedAt  time.Time `xorm:"started_at"`
	FinishedAt time.Time `xorm:"finished_at"`
	FeeCents   int       `xorm:"fee_cents"`
}

func (o *Order) Validate() error {
	if o.Distance <= 0 {
		return OrderDistanceInvalidError
	}

	if o.FinishedAt.Before(o.StartedAt) {
		return OrderFinishedAtInvalidError
	}

	return nil
}
