## Taxi

```
出租车起步价14元，含3公里
起步价之后，每公里2.5元
晚上11点之后（含），次日6点前（不含）起步价18元，含3公里。计价以上车时间为准，不考虑乘坐期间从白天到晚上的情况。
晚上起步价之后，每公里3元
10公里之后，白天每公里3.5元，晚上每公里4.7元
外环的出租车10公里之外的价格与10公里之内相同。外环与内环是出租车的属性，也就是说一辆外环的出租车在内环拉了客人，并行使到了外环，整段旅程都是外环的计费策略。
```

### Project Structure

```
├── Makefile
├── README.md
├── bin // 项目生成的二进制文件
│   └── taxi
├── cmd // 项目的可执行文件
│   └── httpserver
│       └── server.go
├── go.mod
├── go.sum
├── http // HTTP 入口
│   ├── handler // HTTP handler, 处理具体业务
│   │   └── order
│   │       ├── order.go
│   │       └── order_test.go
│   ├── service.go // HTTP Service 启动配置
│   └── service_option.go // HTTP Service 启动配置
├── initializer // 项目初始化入口, 做为全局引用的库的入口, 有效避免在 Go 中出现循环引用的问题
│   └── intializer.go
├── model // 放置 project 核心的 model struct and functions
│   ├── order.go
│   ├── order_test.go
│   └── taxi.go
├── pkg // 用来放置库代码，可被项目内部或外部引用
│   └── billing // 打车价格计费模块
│       ├── billing.go
│       ├── billing_test.go
│       └── billingmock // mock 包, 用于外部 package 测试
│           └── biller.go
└── store // 数据存储层
    ├── memstore.go
    └── storemock // mock 包, 用于外部 package 测试
        └── storer.go
```

### Requirements

- golang 1.12

### Setup

- go get ./...

### Build

- make build

### Test

> 核心功能均有测试

- make test

### Run

- make build && bin/taxi

### Design

- model/taxi 用来表示出租车实体
- model/order 用来表示一个订单实体
- pkg/billing 模块用于计算费用
- store 用于存储数据, 用于时间问题, 简化为内存存储, 由于对外部是以 interface 的形式暴露的, 后续如果要接 DB 只需要增加基于 DB 实现 interface 的代码即可, 无需修改业务代码

计算的流程大致如下:

1. 分别在计费模块配置白天、夜间的时间范围以及价格范围，通知设置了连个阈值 `NormalWaterMark` 和 `HighMaterMark`
    - `NormalWaterMark` 用于表示起步价范围的上限
    - `HighWaterMark` 用于表示路程到达指定距离后, 开始增加每公里费用的阈值
    - `HighWaterMarkTaxiRingTypes` 配置为数组, 方便后续增加、减少距离超过 `HighWaterMark` 后需要增加收费的车型
    
2. 一笔订单完成后, 先根据起始时间扣除起步费
3. 再根据距离，扣除 `NormalWaterMark` 和 `HighWaterMark` 之间的费用
4. 最后扣除大于 `HighWaterMark` 距离的费用

### API Doc

#### 开始订单
> POST api/v1/orders

##### Params
```json
{
  "ringType": 0
}
```

##### Response
> Code: 201
```json
{
  "orderId":   1,
  "startedAt": 1564566563,
  "ringType":  0
}
```

#### 结束订单
> PUT api/v1/orders/:orderId

##### Params
```json
{
  "distance": 100
}
```

##### Response
> Code: 200
```json
{
  "orderId":    1,
  "finishedAt": 1564566563,
  "feeCents":   1000
}
```