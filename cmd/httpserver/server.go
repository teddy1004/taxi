package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/teddy1004/taxi/http"
	"github.com/teddy1004/taxi/initializer"
)

func main() {
	initializer.InitHTTPService()

	hs := http.NewService(
		"127.0.0.1:8080",
		http.WithGinRouteOption(),
	)
	defer hs.Close()

	go func() {
		if err := hs.Run(); err != nil {
			log.Fatalf("http server Listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	signal.Reset(os.Interrupt, syscall.SIGTERM, syscall.SIGKILL)
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM, syscall.SIGKILL)
	<-quit
	logrus.Info("server shutdown...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := hs.Shutdown(ctx); err != nil {
		log.Fatal("server shutdown:", err)
	}
	logrus.Info("server exiting")
}
